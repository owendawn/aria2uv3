/**
 * @type {import('@vue/cli-service').ProjectOptions}
 */
module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? '././' : '/',
    configureWebpack: {
        externals: {
            "vant": "vant",
            "echarts": "echarts"
        }
    },
}