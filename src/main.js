import {createApp} from 'vue'
import router from './route'
import vant from 'vant';
import vuex from 'vuex';
import App from './App.vue'
import {Button,Progress,SwipeCell,Cell,Tabs,Tab,CellGroup,Field,Form,Popup,
    Picker,Slider,Sidebar,SidebarItem,Checkbox,Grid,GridItem} from 'vant';
import store from '@/store'

const app = createApp(App)
app.use(vuex);
app.use(vant);
app.use(Button);
app.use(Progress);
app.use(SwipeCell);
app.use(Cell);
app.use(Tabs);
app.use(Tab);
app.use(CellGroup);
app.use(Field);
app.use(Form);
app.use(Popup);
app.use(Picker);
app.use(Slider);
app.use(Sidebar);
app.use(SidebarItem);
app.use(Checkbox);
app.use(Grid);
app.use(GridItem);

app.use(store).use(router).mount('#app')